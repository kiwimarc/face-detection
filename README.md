**In Beta state!!**

# Face detection

Python script that can recognize faces and access a database to get the name of the regonize person.

# Requirements
* nodejs server
* Apache server
* mysql server
* python 3.7

# Features
*  Regonize people from 1 data point
*  Access database to get names

# Up comming features
*  Optimize for better stability and less memory hungry

# Contributors
[Kiwimarc](https://gitlab.com/kiwimarc)  
[Ninjajen](https://gitlab.com/Ninjajen)
