<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-16">
    <title>Create User</title>
	<link href="assets/style.css" rel="stylesheet" type="text/css">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
</head>
<body>
	<h2 class="login"> Sign up </h2>
    <form action="assets/signup.php" method="POST" enctype="multipart/form-data">
		<input type="text" name="first" placeholder="First name">
        <input type="text" name="last" placeholder="Last name">
		<p>Role:</p>
        <select name="role" class="dropdown-content">
		  <option value="student">Student</option>
		  <option value="teacher">Teacher</option>
		  <option value="admin">Admin</option>
		</select>
		<p>Select image to upload:</p>
		<input type="file" name="file" id="file">
		<p>Class:</p>
        <select name="class" class="dropdown-content">
		  <option value="1.1">1.1</option>
		  <option value="1.2">1.2</option>
		  <option value="1.3">1.3</option>
		</select>
		<input type="number" name="cpr" placeholder="CPR number">
		<input type="text" name="address" placeholder="Address">
        <button type="submit" id="submit" name="submit">Sign up</button>
    </form>
</body>
</html>
