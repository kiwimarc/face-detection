<?php

// Did user acces file by clicking submit?
if (isset($_POST['submit'])) {

    	include_once 'db.php';

    	//Data from the form
    	// Convert everything to text
    	$role = $conn->real_escape_string($_POST['role']);
	$first = $conn->real_escape_string($_POST['first']);
	$last = $conn->real_escape_string($_POST['last']);
	$class = $conn->real_escape_string($_POST['class']);
	$cpr = $conn->real_escape_string($_POST['cpr']);
	$address = $conn->real_escape_string($_POST['address']);
	$digits = 7;
	$uname = rand(pow(10, $digits-1), pow(10, $digits)-1); // Create random username
	$uname = $conn->real_escape_string($uname);
	$upass = "pass-".substr($cpr, -4,); // Create password from cpr number
	$upass = $conn->real_escape_string($upass);
	$target_dir = "uploads/".date('Y')."/";
	if ( !file_exists($target_dir) ) {
		mkdir ($target_dir, 0744); // Makes year dir if dir not exist
 	}
	$file_rename = $uname . substr($_FILES["file"]["name"], strripos($_FILES["file"]["name"], '.')); // Rename image to uname
	$target_file = $target_dir . basename($file_rename);
	$target_file = $conn->real_escape_string($target_file);
	move_uploaded_file($_FILES["file"]["tmp_name"], $target_file); // Uploads file to dir
	$sql = "SELECT * FROM users WHERE uname=".$uname;
	$result = mysqli_query($conn, $sql);
	$resultCheck = mysqli_num_rows($result);
	// Checks if username exist and creates a new one if it does
	if ($resultCheck > 0) {
		while ($resultCheck > 0){
			$uname = rand(7,7);
		}
	} else {
		// hash upass
		$hashedpwd = password_hash($upass, PASSWORD_DEFAULT);
		//Insert user into db
		$sql ="INSERT INTO users (uname, upass, role) VALUES ('$uname', '$hashedpwd', '$role');";
		mysqli_query($conn, $sql);
		$sql = "SELECT id FROM users WHERE uname='$uname'";
		$result = mysqli_query($conn, $sql);
		$row = $result->fetch_array();
		$u_id = $row['id'];
		$sql = "SELECT id FROM class WHERE name='$class'";
		$result = mysqli_query($conn, $sql);
		$row = $result->fetch_array();
		$class_id = $row['id'];
		$sql = "INSERT INTO user_info (u_id, f_name, l_name, class_id, cpr, address, image) VALUES ('$u_id', '$first', '$last', '$class_id', '$cpr', '$address', '$target_file');";
		mysqli_query($conn, $sql);
		mysqli_close($conn); //Make sure to close out the database connection
	}
}
//Disables URL-access and sends back to signup-page
else {
    header("Location: ../index.php");
    exit();
}
