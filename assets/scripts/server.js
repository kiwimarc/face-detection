// Minimal amount of secure websocket server
var fs = require('fs');
// read ssl certificate
var privateKey = fs.readFileSync('ssl-cert/privkey.pem', 'utf8');
var certificate = fs.readFileSync('ssl-cert/fullchain.pem', 'utf8');
var credentials = { key: privateKey, cert: certificate };
var https = require('https');

var express = require('express');
var app = express();

//pass in your credentials to create an https server
var httpsServer = https.createServer(credentials);
httpsServer.listen(8443);
var WebSocketServer = require('ws').Server;
var wss = new WebSocketServer({
    server: httpsServer
}, () => console.log(`WS server is listening at wss://theserver.dk:8443`));

// array of connected websocket clients
let connectedClients = [];

wss.on('connection', (ws, req) => {
    console.log('Connected');
	// add new connected client
	connectedClients.push(ws);
    // listen for messages from the streamer, the clients will not send anything so we don't need to filter
    ws.on('message', data => {
        // send the base64 encoded frame to each connected ws
        connectedClients.forEach((ws, i) => {
            if (ws.readyState === ws.OPEN) { // check if it is still connected
                ws.send(data); // send
            } else { // if it's not connected remove from the array of connected ws
                connectedClients.splice(i, 1);
            }
        });
    });
});
// HTTP stuff
app.get('/absence/recon', (req, res) => res.sendFile(path.resolve(__dirname, 'absence/recon.php')));
app.get('/absence/cam', (req, res) => res.sendFile(path.resolve(__dirname, 'absence/cam.php')));
app.listen(8442, () => console.log(`HTTP server listening at https://theserver.dk:8442`));

function loop() {

const spawn = require("child_process").spawn;
const pythonProcess = spawn('python3',["recon.py"]);
};

function run() {
  setInterval(loop, 2000);
};

run();
