#!/usr/bin/env python
import dbcon
import asyncio
import websockets
import base64
import io
import datetime
import face_recognition
import numpy as np
import matplotlib.pyplot as plt
from os import listdir
from PIL import Image
from cv2 import imshow, imwrite
from cv2 import CascadeClassifier
from cv2 import rectangle
from cv2 import cvtColor, COLOR_BGR2GRAY, COLOR_RGB2BGR

# load the pre-trained models
classifier = CascadeClassifier('haarcascade_frontalface_default.xml')
eye_classifier = CascadeClassifier('haarcascade_eye.xml')
smiles_classifier = CascadeClassifier('haarcascade_smile.xml')

now = datetime.datetime.now()
folder = "/uploads/"+str(now.year)+"/"
name = "Unknown"

# load images and learn how to recognize them
img_list = listdir(folder)
known_imgs = []
known_imgs_names = []
for img in img_list:
    image = face_recognition.load_image_file(folder + img)
    image_encode = face_recognition.face_encodings(image)[0]
    known_imgs.append(image_encode)
    img = str(img.split('.')[0])
    known_imgs_names.append(img)

def readb64(base64_string):
    base64_string = str(base64_string.split(',')[1])
    sbuf = io.BytesIO()
    sbuf.write(base64.b64decode(base64_string))
    pimg = Image.open(sbuf)
    plt.figure()
    plt.imshow(pimg, cmap="gray")
    return cvtColor(np.array(pimg), COLOR_RGB2BGR)

def b64encode(img):
    encoded_string = base64.b64encode(open(img,"rb").read())
    return encoded_string

def recon(frame):
    name = "Unknown"
    # load the photograph
    pixels = cvtColor(frame, COLOR_BGR2GRAY)
    # perform face detection
    faces = classifier.detectMultiScale(pixels, 1.3, 5)
    # print bounding box for each detected face
    for box in faces:
        # extract
        x, y, width, height = box
        x2, y2 = x + width, y + height
        
        face_location = face_recognition.face_locations(frame)
        faces = face_recognition.face_encodings(frame,face_location)
        
        # draw a rectangle over the pixels
        rectangle(frame, (x, y), (x2, y2), (0,0,255), 1)
        # Detects eyes of different sizes in the input image 
        eyes = eye_classifier.detectMultiScale(pixels[y:y2, x:x2], 1.3, 5)
      
        #To draw a rectangle in eyes 
        for (ex,ey,ew,eh) in eyes: 
            rectangle(frame,(ex+x,ey+y),(ew+ex+x,eh+ey+y),(0,127,255),1)
        # Detects smiles of different sizes in the input image 
        smiles = smiles_classifier.detectMultiScale(pixels[y:y2, x:x2], 1.3, 5)  
      
        #To draw a rectangle in smiles 
        for (sx,sy,sw,sh) in smiles: 
            rectangle(frame,(sx+x,sy+y),(sx+sw+x,sy+sh+y),(0,255,255),1)
        
        for face in faces:
            results = face_recognition.compare_faces(known_imgs, face)
            face_distances = face_recognition.face_distance(known_imgs, face)
            best_match_index = np.argmin(face_distances)
            if results[best_match_index]:
                name = known_imgs_names[best_match_index]
    # save the image
    imwrite('image.jpg',frame)
    return name

async def hello():
    async with websockets.connect('wss://theserver.dk:8443') as websocket:
        msg = await websocket.recv()
        cvimg = readb64(msg)
        number = recon(cvimg)
        if(number!="Unknown"):
            name = dbcon.findName(number)
        else:
            name="Unknown"
        string = b64encode('image.jpg')
        string = string.decode('utf8')
        await websocket.send("img."+name+".data:image/png;base64,"+string)
asyncio.get_event_loop().run_until_complete(hello())


